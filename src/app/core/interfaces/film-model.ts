import { Language } from 'src/models/language.model';
import { Inventory } from 'src/models/inventory.model';
import { Category } from 'src/models/category.model';

export interface FilmModel {

  title: string;

	description: string;

	releaseYear: number;

	language: Language;

	originalLanguagId: number;

	rentalDuration: number;

	rentalRate: number;

	length: number;

	replacementCost: number;

	rating: string;

	specialFeatures: string;

	actors: [];

	category: Category;

	inventory: Inventory;

	image: [];

}
